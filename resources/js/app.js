
require('./bootstrap');

window.Vue = require('vue');



import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';


import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import es from 'vee-validate/dist/locale/es.json';
import * as rules from 'vee-validate/dist/rules';


Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize('es', es);

//pensamientos

// test component
Vue.component('my-thoughts-component', require('./components/MyThoughtsComponent.vue').default);
Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('thought-component', require('./components/ThoughtComponent.vue').default);
Vue.component('app-component', require('./components/AppComponent.vue').default);
Vue.component('post-component', require('./components/PostComponent.vue').default);


Vue.component('datepicker', DatePicker );
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

import router from './routes';

const app = new Vue({
    router
}).$mount('#app');
