import Vue from 'vue';
import Router from 'vue-router';


Vue.use(Router)

export default new Router({
    routes:[
        {
            path: '/home',
            name:'home',
            component: require('./views/Home.vue').default
        },
        {
            path: '/blog',
            name:'blog',
            component: require('./views/Blog.vue').default
        },
        {
            path: '/my-thoughts',
            name:'my-thoughts',
            component: require('./components/MyThoughtsComponent.vue').default
        },
        {
            path: '/*',
            component: require('./views/404.vue').default,
        },
    ],
    mode: 'history' // #/blog /#home evita el #
})