<?php
// use App\Http\Controllers\API\ThoughtController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('test/', function () {
    return 'User ';
});

Route::apiResource('thoughts', 'API\ThoughtController');

